import wx
import os
APPNAME = "TorchTester"


def clear(wxObj):
    wx.CallAfter(wxObj.flow1.SetLabel, '')
    wx.CallAfter(wxObj.flow2.SetLabel, '')
    wx.CallAfter(wxObj.flow3.SetLabel, '')
    wx.CallAfter(wxObj.flow4.SetLabel, '')
    wx.CallAfter(wxObj.leak1.SetLabel, '')
    wx.CallAfter(wxObj.leak2.SetLabel, '')
    wx.CallAfter(wxObj.leak3.SetLabel, '')
    wx.CallAfter(wxObj.leak4.SetLabel, '')


    wx.CallAfter(wxObj.imgFlow1.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgFlow1.SetSize, (72,72))

    wx.CallAfter(wxObj.imgFlow2.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgFlow2.SetSize, (72,72))

    wx.CallAfter(wxObj.imgFlow3.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgFlow3.SetSize, (72,72))
    
    wx.CallAfter(wxObj.imgFlow4.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgFlow4.SetSize, (72,72))
    
    wx.CallAfter(wxObj.imgLeak1.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgLeak1.SetSize, (72,72))
    
    wx.CallAfter(wxObj.imgLeak2.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgLeak2.SetSize, (72,72))
    
    wx.CallAfter(wxObj.imgLeak3.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgLeak3.SetSize, (72,72))
    
    wx.CallAfter(wxObj.imgLeak4.SetBitmap, (wx.Bitmap(os.path.join(os.environ['APPDATA'], APPNAME) +'\\blank.png')))
    wx.CallAfter(wxObj.imgLeak4.SetSize, (72,72))