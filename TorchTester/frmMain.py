# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Sep  8 2010)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import logic
import thread
import time
import datetime
import subprocess
from wx.lib.wordwrap import wordwrap
###########################################################################
## Class frmMain
###########################################################################

class frmMain ( wx.Frame ):

    
    def __init__( self, parent,settings):
        self.settings = settings
        
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Torch Tester",
            pos = wx.DefaultPosition, size = wx.Size( 925,631 ), style = wx.SYSTEM_MENU | wx.CLOSE_BOX | wx.CAPTION | wx.MINIMIZE_BOX   )
        favicon = wx.Icon(settings.ICON_FILE,
                           wx.BITMAP_TYPE_ICO)
        self.SetIcon(favicon)



        self.Bind(wx.EVT_CLOSE, self.onClose)
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        formSizer = wx.BoxSizer( wx.VERTICAL )
        
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.m_panel2 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        mainSizer.Add( self.m_panel2, 1, wx.EXPAND, 5 )
        
        formSizer.Add( mainSizer, 0, wx.EXPAND, 5 )
        
        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        gSizer3 = wx.GridSizer( 2, 4, 0, 0 )
        
        LeakSizerLeft = wx.BoxSizer( wx.VERTICAL )
        
        leakTitleSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.leakTitle = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Leak", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.leakTitle.Wrap( -1 )
        self.leakTitle.SetFont( wx.Font( 22, 74, 90, 90, True, "Tahoma" ) )
        
        leakTitleSizer.Add( self.leakTitle, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        LeakSizerLeft.Add( leakTitleSizer, 0, wx.EXPAND, 5 )
        
        leakSizer1 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.leak1 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.leak1.Wrap( -1 )
        self.leak1.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgLeak1 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY|wx.ALIGN_RIGHT, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        leakSizer1.Add( self.imgLeak1, 1, wx.ALL, 5)
        leakSizer1.Add( self.leak1, 0, wx.ALL, 5 )
        LeakSizerLeft.Add( leakSizer1, 1, wx.EXPAND, 5 )


        leakSizer2 = wx.BoxSizer( wx.HORIZONTAL )
        self.leak2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.leak2.Wrap( -1 )
        self.leak2.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgLeak2 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        leakSizer2.Add( self.imgLeak2, 1, wx.ALL, 5 )
        leakSizer2.Add( self.leak2, 0, wx.ALL, 5 )
        LeakSizerLeft.Add( leakSizer2, 1, wx.EXPAND, 5 )

        leakSizer3 = wx.BoxSizer( wx.HORIZONTAL )
        self.leak3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.leak3.Wrap( -1 )
        self.leak3.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgLeak3 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        leakSizer3.Add( self.imgLeak3, 1, wx.ALL, 5 )
        leakSizer3.Add( self.leak3, 0, wx.ALL, 5 )
        LeakSizerLeft.Add( leakSizer3, 1, wx.EXPAND, 5 )
        
        leakSizer4 = wx.BoxSizer( wx.HORIZONTAL )
        self.leak4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.leak4.Wrap( -1 )
        self.leak4.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgLeak4 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        leakSizer4.Add( self.imgLeak4, 1, wx.ALL, 5 )
        leakSizer4.Add( self.leak4, 0, wx.ALL, 5 )
        LeakSizerLeft.Add( leakSizer4, 1, wx.EXPAND, 5 )
        
        
        LeakSizerLeft.AddSpacer( ( 0, 150), 1, wx.EXPAND, 5 )
        
        gSizer3.Add( LeakSizerLeft, 0, wx.EXPAND|wx.RIGHT, 5 )
        
        gSizer3.AddSpacer( ( 18, 0), 1, wx.EXPAND, 5 )
        
        FlowSizerRight = wx.BoxSizer( wx.VERTICAL )
        
        flowTitleSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.flowTitle = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Flow", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.flowTitle.Wrap( -1 )
        self.flowTitle.SetFont( wx.Font( 22, 74, 90, 90, True, "Tahoma" ) )
        
        flowTitleSizer.Add( self.flowTitle, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        FlowSizerRight.Add( flowTitleSizer, 0, wx.EXPAND, 5 )
        



        
        flowSizer1 = wx.BoxSizer( wx.HORIZONTAL )
        self.flow1 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.flow1.Wrap( -1 )
        self.flow1.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgFlow1 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        flowSizer1.Add( self.imgFlow1, 1, wx.ALL, 5 )
        flowSizer1.Add( self.flow1, 0, wx.ALL, 5 )
        FlowSizerRight.Add( flowSizer1, 1, wx.EXPAND, 5 )




        flowSizer2 = wx.BoxSizer( wx.HORIZONTAL )
        self.flow2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.flow2.Wrap( -1 )
        self.flow2.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgFlow2 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        flowSizer2.Add( self.imgFlow2, 1, wx.ALL, 5 )
        flowSizer2.Add( self.flow2, 0, wx.ALL, 5 )
        FlowSizerRight.Add( flowSizer2, 1, wx.EXPAND, 5 )

        



        flowSizer3 = wx.BoxSizer( wx.HORIZONTAL )
        self.flow3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.flow3.Wrap( -1 )
        self.flow3.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgFlow3 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        flowSizer3.Add( self.imgFlow3, 1, wx.ALL, 5 )
        flowSizer3.Add( self.flow3, 0, wx.ALL, 5 )
        FlowSizerRight.Add( flowSizer3, 1, wx.EXPAND, 5 )
        


        flowSizer4 = wx.BoxSizer( wx.HORIZONTAL )
        self.flow4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.flow4.Wrap( -1 )
        self.flow4.SetFont( wx.Font( 12, 74, 90, 90, False, "Tahoma" ) )
        self.imgFlow4 = wx.StaticBitmap( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        flowSizer4.Add( self.imgFlow4, 1, wx.ALL, 5 )
        flowSizer4.Add( self.flow4, 0, wx.ALL, 5 )
        FlowSizerRight.Add( flowSizer4, 1, wx.EXPAND, 5 )
        
        
        FlowSizerRight.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        gSizer3.Add( FlowSizerRight, 0, wx.EXPAND|wx.LEFT, 5 )
        
        self.m_panel1.SetSizer( gSizer3 )
        self.m_panel1.Layout()
        gSizer3.Fit( self.m_panel1 )
        formSizer.Add( self.m_panel1, 1, wx.EXPAND, 5 )
        
        
        formSizer.AddSpacer( ( 1024 , 0), 0, wx.EXPAND, 5 )
        
        self.SetSizer( formSizer )
        self.Layout()
        self.mnuMainBar = wx.MenuBar( 0 )
        self.mnuOptions = wx.Menu()

        self.m_sqlite = wx.MenuItem( self.mnuOptions, wx.ID_ANY, u"SQLite Browser", wx.EmptyString, wx.ITEM_NORMAL )
        self.Bind(wx.EVT_MENU, self.sqlite_browser, self.m_sqlite)    
        self.mnuOptions.AppendItem( self.m_sqlite )


        self.about = wx.MenuItem( self.mnuOptions, wx.ID_ANY, u"&About", wx.EmptyString, wx.ITEM_NORMAL )
        self.Bind(wx.EVT_MENU, self.about_button, self.about)
        self.mnuOptions.AppendItem( self.about )



        self.m_exit = wx.MenuItem( self.mnuOptions, wx.ID_ANY, u"E&xit", wx.EmptyString, wx.ITEM_NORMAL )
        self.Bind(wx.EVT_MENU, self.onClose, self.m_exit)    
        self.mnuOptions.AppendItem( self.m_exit )

        self.mnuMainBar.Append( self.mnuOptions, u"&Options" ) 
        self.SetMenuBar( self.mnuMainBar )
        
        #self.m_statusBar1 = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
        
        self.Centre( wx.BOTH )
        thread.start_new_thread(WorkerThread,(self,settings,))
    
    def __del__( self ):
        self.destroy()
        pass

    def onClose(self,event):
        self.Destroy()


    def about_button(self, evt):
       # First we create and fill the info object
       info = wx.AboutDialogInfo()
       info.Name = "TorchTester"
       info.Version = "1.0.0.1"
       info.Copyright = "(C) 2011 Benchtop Devices LLC"
       info.Description = wordwrap("""
This program monitors the CTS Blackbelt.
In the left column are leak tests, and on the right are flow tests.
The tests are compared with default values found in config.ini (an example of which is provided for you in the %APPDATA%/TorchTester/ directory)

In the config.ini file, the leak value is the highest value to pass, and the flow is the lowest value to pass.

To view the database, select SQLite Browser from the Options menu.

           """,
           # change the wx.ClientDC to use self.panel instead of self
           350, wx.ClientDC(self))  
       info.WebSite = ("http://www.benchtopdevices.com", "Benchtop Devices")
       info.Developers = [ "Tyrel Souza",
                           "Anthony Souza"]


       # Then we call wx.AboutBox giving it that info object
       wx.AboutBox(info)


    def sqlite_browser(self,event):
        p1=subprocess.Popen(self.settings.DIR + '\\SQLiteBrowser\\SQLiteBrowser.exe "'+self.settings.DB_FILE+'"')



def WorkerThread(self,settings):
    while True:
        logic.run_test(self,settings)
