"""
 Author: Tyrel Souza
 	<tyrel@tyrelsouza.com>
 TO DO:
    Watch for "Stop Button Pressed"
    if test is pass, show colors red and green
    If index of combo box is changed,
          change all visible combo boxes.
    Box for Name
    Map Button
       (have defaults saved)
       pop up page to enter in Progra Number, Test Number,  Set target sccm
       Sync button on bottom
                ex:   write " asdfjasdf jsadf js   #2\r\n"
                      write " asdfsadjf askjdf kasf 3.0"
    deferreds
"""
import wx
import sqlite3 as sqlite
import logic
import serial
import scanports
import os
import io
import ConfigParser
import pdb
import sys
from threading import *
from frmMain import frmMain

EXITMETHOD = ["Unknown","Completed","Stop Button","Severe Leak"]
global DEBUG
DEBUG = True
APPNAME = "TorchTester"

class settings():
    COMPORT = "COM3" # This is the default comport.
    BAUDRATE = 115200
    BYTESIZE = serial.EIGHTBITS
    PARITY = serial.PARITY_NONE
    STOPBITS = serial.STOPBITS_ONE
    DIR = os.path.join(os.environ['APPDATA'], APPNAME)
    CONFIG_FILE = os.path.join(os.environ['APPDATA'], APPNAME) + '\\config.ini'
    DB_FILE = os.path.join(os.environ['APPDATA'], APPNAME) + '\\Torch.sqlite'
    ICON_FILE = os.path.join(os.environ['APPDATA'], APPNAME) + "\HyperTherm.ico"

    def __init__(self):
        if not os.path.exists(self.DIR):
            os.makedirs(self.DIR)
            with open(self.CONFIG_FILE,'w') as f:
                inifile = """[LEAK]
LEAKA = 2.0
LEAKB = 2.0 
LEAKC = 2.0
LEAKD = 2.0
[FLOW]
FLOWA = 28000
FLOWB = 28000
FLOWC = 28000
FLOWD = 78000"""
                f.write(inifile)
                f.close
        

    def get_limits(self,runno=None,settings=None):
        try:
            parser = ConfigParser.ConfigParser()
            parser.read(settings.CONFIG_FILE)
            limits = []
            limits.append([None,parser.get('LEAK','LEAKA')])
            limits.append([None,parser.get('LEAK','LEAKB')])
            limits.append([None,parser.get('LEAK','LEAKC')])
            limits.append([None,parser.get('LEAK','LEAKD')])
            limits.append([None,parser.get('FLOW','FLOWA')])
            limits.append([None,parser.get('FLOW','FLOWB')])
            limits.append([None,parser.get('FLOW','FLOWC')])
            limits.append([None,parser.get('FLOW','FLOWD')])
        except:
            limits = []
            limits.append(['LEAKA','2.0'])
            limits.append(['LEAKB','2.0'])
            limits.append(['LEAKC','2.0'])
            limits.append(['LEAKD','2.0'])
            limits.append(['FLOWA','28000'])
            limits.append(['FLOWB','28000'])
            limits.append(['FLOWC','28000'])
            limits.append(['FLOWD','78000'])

        limit = limits[runno][1]      


        return limit

def main(settings):
    app = wx.App()
    frame = frmMain(None,settings)
    frame.Show()
    app.SetTopWindow(frame)
    app.MainLoop()    
    
def check_db(s):
    """
    This function will select the LAST set comport in the database.
    If there is no last set comport, it will prompt you for your comports, 
    then it will accept the comport and save it to the database.
    """
    conn = sqlite.connect(s.DB_FILE,detect_types=sqlite.PARSE_DECLTYPES)
    c = conn.cursor()
    try:
        c.execute('SELECT CASE WHEN tbl_name = "torch_record" THEN 1 ELSE 0 END FROM sqlite_master WHERE tbl_name = "torch_record" AND type = "table"')
        table_exists = c.fetchone()
        if not table_exists:
           
            c.execute("CREATE TABLE torch_record (id INTEGER PRIMARY KEY AUTOINCREMENT, port_number TEXT, program_number TEXT, test_date DATE, test_time TEXT, measurement REAL,result TEXT)");
        c.execute('SELECT CASE WHEN tbl_name = "torch_record" THEN 1 ELSE 0 END FROM sqlite_master WHERE tbl_name = "torch_record" AND type = "table"')
        table_exists = c.fetchone()

    except sqlite.OperationalError, e:
        print e
    c.close()
    return s

def run_test(settings):
    ser = logic.connect_serial_and_open(settings)
    try:
        # listen to what's happeneing on the serial port.
        EM,out = logic.read_serial(ser)
    except Exception, ex:
        ser.close()
    try:
        ser.close()
    except:
        pass
    return EM,out

if __name__ == "__main__":
    settings = settings()
    settings = check_db(settings)
    main(settings)
       
